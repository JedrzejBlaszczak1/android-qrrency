package com.jblaszczak.qrrency

import com.jblaszczak.qrrency.ui.list.DayRatesAdapter
import com.jblaszczak.qrrency.ui.list.ListItem
import com.jblaszczak.qrrency.ui.list.TYPE_HEADER
import com.jblaszczak.qrrency.ui.list.TYPE_RECORD
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner

@RunWith(RobolectricTestRunner::class)
class DayRatesAdapterTest {

    lateinit var adapter: DayRatesAdapter

    private val items = ArrayList<ListItem>()

    @Before
    fun setup() {
        adapter = DayRatesAdapter()
        items.addAll(arrayListOf(ListItem("Date", TYPE_HEADER), ListItem("Rate", TYPE_RECORD)))
    }

    @Test
    fun isAdapterProperlyBindingValues() {
        adapter.updateItems(items)
        assertEquals(adapter.getItems()[0].type, items[0].type)
        assertEquals(adapter.getItems()[0].value, items[0].value)
        assertEquals(adapter.getItems()[1].type, items[1].type)
        assertEquals(adapter.getItems()[1].value, items[1].value)
    }
}