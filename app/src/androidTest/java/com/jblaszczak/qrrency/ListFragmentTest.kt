package com.jblaszczak.qrrency

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.jblaszczak.qrrency.ui.list.DayRatesAdapter
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class ListFragmentTest {

    @get: Rule
    val activityRule: ActivityScenarioRule<MainActivity> =
        ActivityScenarioRule(MainActivity::class.java)

    @Test
    fun isRecyclerViewVisible() {
        onView(withId(R.id.includeRecyclerList)).check(matches(isDisplayed()))
    }

    @Test
    fun enteringDetails() {
        Thread.sleep(2000)
        onView(withId(R.id.includeRecyclerList)).perform(
            RecyclerViewActions.actionOnItemAtPosition<DayRatesAdapter.ViewHolderRecord>(
                2,
                click()
            )
        )
        onView(withId(R.id.layoutItem)).check(matches(isDisplayed()))
        ViewActions.pressBack()
    }
}