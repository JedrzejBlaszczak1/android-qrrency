package com.jblaszczak.qrrency.network

import com.jblaszczak.qrrency.network.response.DayRates
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

const val API_KEY = "fda53380cbc5c95d28ff19a70768d5ce"
const val BASE_URL = "http://data.fixer.io/api/"
const val SET_OF_DATA_SIZE = 10L

interface CurrencyApiService {

    @GET("{date}?access_key=$API_KEY")
    suspend fun getHistoricalRates(
        @Path("date") date: String,
        @Query("symbols") symbols: String
    ): DayRates

}

private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

private val retrofit =
    Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .baseUrl(BASE_URL)
        .build()

object CurrencyApi {
    val currencyApiService: CurrencyApiService by lazy {
        retrofit.create(CurrencyApiService::class.java)
    }
}