package com.jblaszczak.qrrency.di

import com.jblaszczak.qrrency.ui.list.DayRatesAdapter
import com.jblaszczak.qrrency.ui.list.ListViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ListModule {
    @Provides
    fun provideAdapter(): DayRatesAdapter{
        return DayRatesAdapter()
    }
}