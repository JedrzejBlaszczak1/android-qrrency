package com.jblaszczak.qrrency

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class QrrencyApplication : Application() {
}