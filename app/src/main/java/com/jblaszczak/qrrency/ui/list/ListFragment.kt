package com.jblaszczak.qrrency.ui.list

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.jblaszczak.qrrency.R
import com.jblaszczak.qrrency.addOnScrolledToEnd
import com.jblaszczak.qrrency.databinding.FragmentListBinding
import com.jblaszczak.qrrency.ui.list.details.ARG_ITEM_CURRENCY
import com.jblaszczak.qrrency.ui.list.details.ARG_ITEM_DAY
import com.jblaszczak.qrrency.ui.list.details.ListItemDetailsActivity
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment : Fragment(), DayRatesAdapter.OnItemListener {
    private lateinit var binding: FragmentListBinding

    private val viewModel: ListViewModel by activityViewModels()

    @Inject
    lateinit var adapter: DayRatesAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (viewModel.dayRatesList.value?.isEmpty() == true)
            viewModel.fetchNextSetOfDays()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("onCreateView() called")

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated() called")

        binding.includeRecyclerList.recyclerList.layoutManager = LinearLayoutManager(context)
        binding.includeRecyclerList.recyclerList.addItemDecoration(
            DividerItemDecoration(
                context,
                DividerItemDecoration.VERTICAL
            )
        )

        adapter.setOnItemListener(this)
        binding.includeRecyclerList.recyclerList.adapter = adapter

        viewModel.dayRatesList.observe(viewLifecycleOwner) { items ->
            items.let {
                Timber.d("viewModel.dayRatesList.observe(): items: %s", items.toString())
                val newItems = ArrayList<ListItem>()
                for (item in items) {
                    newItems.add(ListItem(item.date, TYPE_HEADER))

                    for (rate in item.rates) {
                        newItems.add(ListItem(rate.key + " : " + rate.value, TYPE_RECORD))
                    }
                }
                adapter.updateItems(newItems)
            }
        }

        binding.includeRecyclerList.recyclerList.addOnScrolledToEnd {
            viewModel.fetchNextSetOfDays()
            Timber.d("onScrolledToEnd")
        }

    }

    override fun onItemClick(position: Int) {
        Timber.d("onItemClick")

        val itemsList = adapter.getItems()
        var helperCounter = position
        var dateOfClickedItem = ""

        while (itemsList[helperCounter].type != TYPE_HEADER) {
            helperCounter--
        }

        dateOfClickedItem = itemsList[helperCounter].value

        val intent = Intent(context, ListItemDetailsActivity::class.java)
        intent.putExtra(ARG_ITEM_DAY, dateOfClickedItem)
        intent.putExtra(ARG_ITEM_CURRENCY, itemsList[position].value)
        startActivity(intent)
    }
}