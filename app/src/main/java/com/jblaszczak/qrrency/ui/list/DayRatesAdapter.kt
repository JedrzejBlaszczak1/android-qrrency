package com.jblaszczak.qrrency.ui.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jblaszczak.qrrency.databinding.ListItemHeaderBinding
import com.jblaszczak.qrrency.databinding.ListItemRecordBinding
import timber.log.Timber
import java.lang.IllegalStateException

class DayRatesAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val items = ArrayList<ListItem>()
    private lateinit var onItemListener: OnItemListener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        Timber.d("viewType: %s", viewType)
        return if (viewType == TYPE_HEADER)
            ViewHolderHeader(ListItemHeaderBinding.inflate(inflater))
        else
            ViewHolderRecord(ListItemRecordBinding.inflate(inflater), onItemListener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
       when(getItemViewType(position)){
           TYPE_HEADER -> (holder as ViewHolderHeader).bind(items[position])
           TYPE_RECORD -> (holder as ViewHolderRecord).bind(items[position])
           else -> IllegalStateException("Unsupported item type")
       }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type
    }

    fun updateItems(newItems: List<ListItem>) {
        Timber.d("updateItems() : Items count to be added = %s", newItems.size)
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun setOnItemListener(listener: OnItemListener) {
        this.onItemListener = listener
    }

    fun getItems(): ArrayList<ListItem> {
        return items
    }

    inner class ViewHolderHeader(
        val binding: ListItemHeaderBinding
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        fun bind(item: ListItem?) {
            binding.item = item
            binding.executePendingBindings()
        }

        override fun onClick(p0: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    inner class ViewHolderRecord(
        val binding: ListItemRecordBinding,
        private val newOnItemListener: OnItemListener
    ) : RecyclerView.ViewHolder(binding.root), View.OnClickListener {

        lateinit var onItemListener: OnItemListener

        fun bind(item: ListItem?) {
            binding.item = item
            onItemListener = newOnItemListener
            binding.textViewRate.setOnClickListener(this)
            binding.executePendingBindings()
        }

        override fun onClick(p0: View?) {
            onItemListener.onItemClick(adapterPosition)
        }
    }

    interface OnItemListener {
        fun onItemClick(position: Int)
    }
}