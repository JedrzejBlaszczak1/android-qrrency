package com.jblaszczak.qrrency.ui.list.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.jblaszczak.qrrency.R
import com.jblaszczak.qrrency.databinding.FragmentListItemDetailsBinding
import com.jblaszczak.qrrency.ui.list.ListItem
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

const val ARG_ITEM_DAY = "arg_item_day"
const val ARG_ITEM_CURRENCY = "arg_item_currency"

@AndroidEntryPoint
class ListItemDetailsFragment : Fragment() {

    private lateinit var binding: FragmentListItemDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        Timber.d("onCreateView()")
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list_item_details, container, false)

        binding.item = ListDetailsItem(arguments?.getString(ARG_ITEM_DAY)!!, arguments?.getString(ARG_ITEM_CURRENCY)!!)

        return binding.root
    }
}