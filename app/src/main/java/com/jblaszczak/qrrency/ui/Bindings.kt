package com.jblaszczak.qrrency.ui

import android.widget.TextView
import androidx.databinding.BindingAdapter
import java.time.LocalDate

class Bindings{
    companion object{
        @BindingAdapter("itemDateValue")
        @JvmStatic
        fun setItemDate(textView: TextView, date: String?){
            if(date!=null){
                val localDate = LocalDate.parse(date)
                """Dzień ${if(localDate.dayOfMonth<10)"0"+localDate.dayOfMonth else localDate.dayOfMonth}-${if(localDate.monthValue<10)"0"+localDate.monthValue else localDate.monthValue}-${localDate.year}""".also { textView.text = it }
            }
        }
    }
}