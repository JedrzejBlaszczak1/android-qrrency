package com.jblaszczak.qrrency.ui.list

const val TYPE_HEADER=0
const val TYPE_RECORD=1

data class ListItem(val value: String, val type: Int)