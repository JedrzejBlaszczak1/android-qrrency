package com.jblaszczak.qrrency.ui.list.details

data class ListDetailsItem(val date: String, val currency: String)