package com.jblaszczak.qrrency.ui.list.details

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.jblaszczak.qrrency.R
import com.jblaszczak.qrrency.databinding.ActivityListItemDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import timber.log.Timber

@AndroidEntryPoint
class ListItemDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityListItemDetailsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("onCreate")
        binding = DataBindingUtil.setContentView(this, R.layout.activity_list_item_details)

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationOnClickListener {
            Timber.d("OnNavPressed")
            finish()
        }

        if (savedInstanceState == null) {
            Timber.d("savedInstanceNULL")
            val fragment = ListItemDetailsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_ITEM_DAY, intent.getStringExtra(ARG_ITEM_DAY))
                    putString(ARG_ITEM_CURRENCY, intent.getStringExtra(ARG_ITEM_CURRENCY))
                }
            }

            supportFragmentManager.beginTransaction()
                .add(R.id.item_details_container, fragment)
                .commit()
        }
    }

    override fun onBackPressed() {
        Timber.d("OnBackPressed")
        this.finish()
    }
}