package com.jblaszczak.qrrency.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jblaszczak.qrrency.network.CurrencyApi
import com.jblaszczak.qrrency.network.SET_OF_DATA_SIZE
import com.jblaszczak.qrrency.network.response.DayRates
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class ListViewModel @Inject constructor() : ViewModel() {

    val dayRatesList: MutableLiveData<ArrayList<DayRates>>

    init {
        Timber.d("init")
        dayRatesList = MutableLiveData()
        dayRatesList.value = ArrayList()
    }

    fun fetchNextSetOfDays() {
        viewModelScope.launch {
            val newList = dayRatesList.value
            val currentSize = dayRatesList.value?.size
            if (currentSize != null) {
                for (i in currentSize..currentSize + SET_OF_DATA_SIZE) {
                    newList?.add(
                        CurrencyApi.currencyApiService.getHistoricalRates(
                            LocalDateTime.now().minusDays(i).format(DateTimeFormatter.ISO_DATE),
                            "usd,pln,aud"
                        )
                    )
                }
                dayRatesList.postValue(newList)
            }
            Timber.d(dayRatesList.value.toString())
        }
    }

}